/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const helloWorld = `query HelloWorld($consumer_key: String, $consumer_secret: String) {
  helloWorld(consumer_key: $consumer_key, consumer_secret: $consumer_secret)
}
`;
export const getMessages = `query GetMessages($filter: String) {
  getMessages(filter: $filter) {
    messageId
    body
    createdAt
    handle
  }
}
`;
