export default {
  getList(state) {
    return state.list
  },
  getContactsData(state) {
    const contacts = state.list.filter(
      (contact) => contact.type === state.typeIsContact.type
    )

    if (state.typeIsContact.query.length > 2) {
      return contacts.filter((contact) =>
        contact.reason.toLowerCase().includes(state.typeIsContact.query)
      )
    }

    return contacts
  },
  getProvidersData(state) {
    const providers = state.list.filter(
      (provider) => provider.type === state.typeIsProvider.type
    )

    if (state.typeIsProvider.query.length > 2) {
      return providers.filter((provider) =>
        provider.reason.toLowerCase().includes(state.typeIsProvider.query)
      )
    }

    return providers
  }
}
