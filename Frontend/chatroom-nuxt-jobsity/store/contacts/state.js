import SET_CONTACTS from './contacts.json'

export default () => ({
  list: SET_CONTACTS,
  typeIsContact: {
    query: '',
    type: true
  },
  typeIsProvider: {
    query: '',
    type: false
  }
})
