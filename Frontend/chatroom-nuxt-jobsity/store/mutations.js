import { TOGGLE_MENU, SET_CURRENT_USER } from './mutation-types'

export default {
  [TOGGLE_MENU](state) {
    state.menu.active = !state.menu.active
  },
  [SET_CURRENT_USER](state, user) {
    state.user.username = user
  }
}
