export default {
  menuIsActive(state) {
    return state.menu.active
  },
  getcurrentUser(state) {
    return state.user.username
  }
}
