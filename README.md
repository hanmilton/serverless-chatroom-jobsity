# Serverless Chatroom Jobsity Challenge

## LIVE DEMO

[https//www.worldchilli.com](https://www.worldchilli.com/)

### User For Demo

- Username: UserChat1 Password: Quien7#Dice
- Username: UserChat2 Password: Quien7#Dice
- Username: UserChat3 Password: Quien7#Dice

## PWA install on Desktop

![PwsDesktop](DocumentationImages/installPWADesktop.png)

## PWA install on Mobile Device

![PwaMobile](DocumentationImages/installMobilePwa.jpeg)

## Chatbot Funcionality

for chatbot execute command like **Stock of name*of_company_here*** example

![Chatbot Funcionality](DocumentationImages/chatBotFuncionality.png)

### List of AWS services

Video reference for configuration aws certificate and s3 deploy [Video1](https://www.youtube.com/watch?v=_Jw6tHUp6js&t=302s) and [video2](https://www.youtube.com/watch?v=uwgB_sIhIko&t=157s)

- **Certificate Manager** for SSL/TLS certificates
- **S3** Bucket here deploy de pwa web app
- **CloudFront** speeds up distribution of your static and dynamic web content, such as . html, . css, . js, and image files
- **Route 53** Domain Name System (DNS) web service

## Previous requirements

### AWS account

Visit [AWS](https://aws.amazon.com/) and create account

### Nodejs and npm

Install [Nodejs](https://nodejs.org/es/) recomended the LTS version

### Serverless Framework

Documentation [Serverless](https://serverless.com/)

```bash
npm install -g serverless
serverless --version
```

### Install and configure AWS CLI profile

Documentation for install AWS CLI [Instalation](https://docs.aws.amazon.com/cli/latest/userguide/install-macos.html)

```bash
aws configure
AWS Access Key ID [None]: YOUR_KEY_ID_AWS
AWS Secret Access Key [None]: YOUR_SECRET_KEY_AWS
Default region name [None]: YOUR_REGION_HERE
Default output format [None]: json
```

## Serverless Backend 💻

> Tech Stack:

- **Serverless Framework** Serverless for building and operating serverless applications.
- **Cognito** Control user authentication and access.
- **Appsync (GraphQL)** Create flexible API with **GraphQL** a query language for API.
- **DynamoDB** NoSQL database service.
- **Lambda (λ)** Run code without provisioning or managing servers.
- **S3** Bucket on Aws.

## Serverless Frontend 💻

- **Vue** Progressisve framework for building user interfaces.
- **Nuxtjs** Framework based on Vue.js this contain libraries like (vue, vue-router and vuex) and for developer experience.
- **Tailwind** CSS framework for rapidly building custon designs.
- **Stylus** stylesheets preprocessor language.
- **Pug** Pug loader for HTML

## Version Control 💻

![Version Control Git Flow](DocumentationImages/versionControl.png)

- **Git** : Distributed version control system.
- **SourceTree**: Git desktop client use **GitFlow** (Master, Develop and Features).
- **Bitbucket**: Manage and share your Git repositories.

## BOT 🚀

- first you need an account on ![Create account on kommunicate](https://www.kommunicate.io/)
- second you need configure your bot on amazon lex ![Amazon Lex](https://aws.amazon.com/es/lex/)

```bash
cd Bot
sls deploy
```

## BackEnd

### Plugins Serverless 🛠

- **serverless-appsync-plugin** Develop AppSync API's locally Serverless
- **serverless-pseudo-parameters** For any of your config strings
- **serverless-stack-output** Plugin to store aws Stack on json file
- **serverless-dotenv-plugin** enviroment variables into serverless
- **serverless-offline-dotenv** .env file located at the root of your proyect
- **serverless-offline** emulates lambda on your local machine
- **serverless-jest-plugin** test driven development

### Env

.env.example you can find a example about your .env on your proyect

### Deploy 🚀

first copy and rename .env.example to .env and modified according your parameters

```bash
cd Backend
npm i
sls deploy
```

### Remove Infra ❌

```bash
sls remove
```

## FrontEnd

- To get started:

```bash
cd Frontend/chatroom-nuxt-jobsity
yarn dev
```

- To build & start for production:

```bash
cd Frontend/chatroom-nuxt-jobsity
yarn build
yarn start
```

- To test:

```bash
cd Frontend/chatroom-nuxt-jobsity
yarn test
```

## Mockups

Differents designs for chilichat:

![Muckup](DocumentationImages/mobile1.jpg)
![Muckup](DocumentationImages/mobile2.jpg)
![Muckup](DocumentationImages/mobile3.jpg)
![Muckup](DocumentationImages/mobile4.jpg)
![Muckup](DocumentationImages/mobile5.jpg)

## Final Design App

![Muckup](DocumentationImages/mobileFinal.png)
![Muckup](DocumentationImages/mobileFinal2.png)
![Muckup](DocumentationImages/mobileFinal3.png)
![Muckup](DocumentationImages/mobileFinal4.png)

## Final Desktop App

![Muckup](DocumentationImages/mobileFinal.png)
![Muckup](DocumentationImages/mobileFinal2.png)
![Muckup](DocumentationImages/mobileFinal3.png)
![Muckup](DocumentationImages/mobileFinal4.png)
![Muckup](DocumentationImages/desktopFinal.png)
![Muckup](DocumentationImages/desktopFinal2.png)
![Muckup](DocumentationImages/desktopFinal3.png)

## Finish Points

- Allow registered users to log in and talk with other users in a chatroom.
- Allow users to post messages as commands into the chatroom with the following format
  /stock=stock_code
- Create a decoupled bot that will call an API using the stock_code as a parameter
  (https://stooq.com/q/l/?s=aapl.us&f=sd2t2ohlcv&h&e=csv, here aapl.us is the
  stock_code)
- The bot should parse the received CSV file and then it should send a message back into
  the chatbot “APPL.US quote is \$93.42 per share”. The post owner will be
  the bot.
  -Have the chat messages ordered by their timestamps and show only the last 50
  messages.

## Not Finish Points

- Using a message broker like RabbitMQ.
