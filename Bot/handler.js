'use strict';
var https = require('https');
function close(sessionAttributes, fulfillmentState, message) {
  return {
    sessionAttributes,
    dialogAction: {
      type: 'Close',
      fulfillmentState,
      message,
    },
  };
}

function resolveStock(company) {
  return new Promise(resolve => {
    var params = {
      host: "stooq.com",
      path: "/q/l/?s=??&f=sd2t2ohlcv&h&e=csv"
    }
    params.path = params.path.replace("??", company)
    var req = https.request(params, function (res) {
      let data = '';
      console.log('STATUS: ' + res.statusCode);
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        resolve(data)
      });
    });
    req.end();
  });
}

async function dispatch(intentRequest, callback) {
  console.log(`request received for userId=${intentRequest.userId}, intentName=${intentRequest.currentIntent.name}`);
  const sessionAttributes = intentRequest.sessionAttributes;
  const slots = intentRequest.currentIntent.slots;
  const messageStock = await resolveStock(slots.company);
  const messageHeaders = messageStock.split(',')
  const configCsv = {
    numOfColums: 8
  };
  let stockObject = {}
  for (var i = 0; i < configCsv.numOfColums; i++) {
    console.log(messageHeaders[i])
    console.log(messageHeaders[i + configCsv.numOfColums - 1])
    stockObject[String(messageHeaders[i])] = messageHeaders[i + configCsv.numOfColums - 1]
  }

  console.log(stockObject)

  console.log(messageHeaders)
  callback(close(sessionAttributes, 'Fulfilled',
    { 'contentType': 'PlainText', 'content': `Okay, Your Stock => Symbol ${stockObject.Symbol} - Date ${stockObject.Date} - Time ${stockObject.Time} - Open ${stockObject.Open} - High ${stockObject.High} - Low ${stockObject.Low} - Close ${stockObject.Close} - Volumen ${stockObject.Volumen}` }));

}

exports.handler = (event, context, callback) => {
  try {
    dispatch(event,
      (response) => {
        callback(null, response);
      });
  } catch (err) {
    callback(err);
  }
};